import { AbstractControl, ValidatorFn, Validators } from '@angular/forms';

export const passwordValidators = [Validators.required, isStrongAs('strong')];

export const strongPasswordLength = 8;
export const strongPasswordRegExp = new RegExp(
  `^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\\W])(?=.{${strongPasswordLength},})`,
);

export const mediumPasswordLength = 6;
export const mediumPasswordRegExp = new RegExp(
  `^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{${mediumPasswordLength},})`,
);

export function isStrongAs(strength: string): ValidatorFn {
  return (c: AbstractControl): null | { isStrong: boolean; msg?: string } => {
    const invalidRes = (msg: string) => ({ isStrong: false, msg });
    try {
      const inputStrength = getStrength(c.value);
      const isValid = inputStrength.toLowerCase() === strength.toLowerCase();
      if (isValid) {
        return null;
      } else {
        return invalidRes(`Password's strength must be at least ${strength}.`);
      }
    } catch (err) {
      return invalidRes('Password validation has failed');
    }
  };
}

export function getStrength(password: string): string {
  if (strongPasswordRegExp.test(password)) {
    return 'strong';
  } else if (mediumPasswordRegExp.test(password)) {
    return 'medium';
  } else if (password.length > 3) {
    return 'weak';
  }
  return '';
}
